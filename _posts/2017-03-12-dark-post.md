---
layout: dark-post
title: Dark Post second
description: "Another sample Dark post"
tags: [sample]
modified: 13.03.2017
---

To use this dark theme, just add:

{% highlight yaml %}
    layout: dark-post
{% endhighlight %}

on your post. This theme adds a dark background with a white panel to show your content.


{% highlight yaml %}
testing, testing...
{% endhighlight %}